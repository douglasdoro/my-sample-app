FROM ruby:2.5.3

RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
ADD config.ru /app/config.ru

RUN bundle install --jobs 4

ADD . /app

CMD ["rackup", "-p", "3000", "--host", "0.0.0.0"]

EXPOSE 3000/tcp 443/tcp
