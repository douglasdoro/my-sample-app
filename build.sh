echo "Remove container \n"
docker rm -f myapp

echo "Remove image \n"
docker rmi -f douglasdoro/mysampleapp

echo "Build image \n"
docker build -t douglasdoro/mysampleapp /Users/douglasdoro/Projects/my-sample-gitlab-app/

echo "Run container \n"
docker run -d -p 3000:3000 --name myapp douglasdoro/mysampleapp:latest 

# echo "Show running container \n"
# docker ps

echo "Result: \n"
if [ $(curl -s -o /dev/null -w "%{http_code}" http://0.0.0.0:3000/api) = '200' ]; then
    echo "Show!"
fi

echo "Finished, doro!"