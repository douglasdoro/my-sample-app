require 'grape'

class API < Grape::API
    version 'v1', using: :header, vendor: 'api'
    format :json
    prefix :api

    get '/' do
        { status: 'ok'}
    end  
    
    get 'time' do
        { time: Time.now }
    end
end